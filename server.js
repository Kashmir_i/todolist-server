const express = require('express'),
    app = express(),
    port = process.env.PORT || 4521,
    mongoose = require('mongoose'),
    Task = require('./api/models/taskModel'),
    Device = require('./api/models/deviceModel'),
    bodyParser = require('body-parser')
;


// mongoose instance connection url connection
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/Tododb');
// mongoose.connect('mongodb+srv://kashmir:<rksdth>@clustertodolist-kew2d.gcp.mongodb.net/test?retryWrites=true&w=majority');


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});


const routes = require('./api/routes/routes'); //importing route
routes(app); //register the route


app.listen(port);


console.log('todo list RESTful API server started on: ' + port);