'use strict';

module.exports = function(app) {
    const todoList = require('../controllers/taskController');
    const device = require('../controllers/deviceController');

    // todoList Routes
    app.route('/api/device/:deviceId/tasks')
        .get(todoList.list_all_tasks)
        .post(todoList.create_a_task);


    app.route('/api/tasks/:taskId')
        .get(todoList.read_a_task)
        .put(todoList.update_a_task)
        .delete(todoList.delete_a_task);

    app.route('/api/tasks/:taskId/notification')
        .put(todoList.add_notification);

    app.route('/api/device')
        .get(device.list_all_device)
        .post(device.create_device);

    app.route('/api/device/:deviceId')
        .get(device.read_device)
        .put(device.update_device)
        .delete(device.delete_device);
};