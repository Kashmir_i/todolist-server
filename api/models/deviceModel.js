'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const DeviceSchema = new Schema({
    platform: {
        type: String,
        required: false
    },
    uuid: {
        type: String,
        required: true,
        unique: true
    },
    version: {
        type: String,
        required: false
    },
    manufacturer: {
        type: String,
        required: false
    },
    isVirtual: {
        type: Boolean,
        required: false
    },
    serial: {
        type: String,
        required: false
    },
    notification_token: {
        type: String,
        required: false
    },
    tasks: [
        { type: mongoose.Schema.Types.ObjectId, ref: 'Tasks' }
    ]
});

module.exports = mongoose.model('Device', DeviceSchema);