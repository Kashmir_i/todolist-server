'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const TaskSchema = new Schema({
    title: {
        type: String,
        required: 'Kindly enter the title of the task'
    },
    description: {
        type: String,
        required: false
    },
    created_at: {
        type: Date,
        default: Date.now
    },
    updated_at: {
        type: Date,
        default: null
    },
    status: {
        type: Boolean,
        default: false
    },
    notification_time: {
        type: Date,
        default: null
    },
    device: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Device'
    }
});

module.exports = mongoose.model('Tasks', TaskSchema);