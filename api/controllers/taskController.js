'use strict';

const mongoose = require('mongoose');
const Task = mongoose.model('Tasks');
const CronJob = require('cron').CronJob;
const Fcm = require('fcm-notification');
const FCM = new Fcm(__dirname+'/todolist-fb234-firebase-adminsdk-8lhjt-1f2cc9986a.json');

let cronJobs = [];

exports.list_all_tasks = function(req, res) {
    let { deviceId } = req.params;
    Task.find({device: deviceId}, function(err, tasks) {
        if (err)
            res.send(err);
        res.json(tasks);
    });
};


exports.create_a_task = function(req, res) {
    let new_task = new Task(req.body);
    new_task.save(function(err, task) {
        if (err)
            res.send(err);
        res.json(task);
    });
};


exports.read_a_task = function(req, res) {
    let { taskId } = req.params;
    Task.findById(taskId, function(err, task) {
        if (err)
            res.send(err);
        res.json([task]);
    });
};


exports.update_a_task = function(req, res) {
    let { taskId } = req.params;
    Task.findOneAndUpdate({_id: taskId}, req.body, {new: true}, function(err, task) {
        if (err) {
            res.send(err);
        } else {
            if (req.body.status) {
                cronJobs.forEach((task) => {
                    if (task._id === taskId) {
                        task['jobs'].forEach(job => {
                            if (job.running)
                                job.stop();
                        });
                        task['jobs'] = [];
                    }
                });
            }

            res.json(task);
        }
    });
};

exports.stop_task_cron_job = function(taskId) {
    cronJobs.forEach((task) => {
        if (task._id === taskId) {
            task['jobs'].forEach(job => {
                if (job.running)
                    job.stop();
            });
            task['jobs'] = [];
        }
    });
};


exports.delete_a_task = function(req, res) {
    let { taskId } = req.params;
    Task.remove({
        _id: taskId
    }, function(err) {
        if (err) {
            res.send(err);
        }
        res.json({ message: 'ok' });
    });
};

exports.add_notification = function (req, res) {
    // this.update_a_task(req, res);

    let { taskId } = req.params;
    let { title, notification_time } = req.body;
    Task.findById(taskId).populate('device').exec(function (err, device) {
        if (err) {
            console.log('err');
        } else {
            console.log('The token is %s', device.device.notification_token);
            let message = {
                android: {
                    priority: 'normal',
                    notification: {
                        title: 'Just do it!',
                        body: title,
                        color: '#f45342'
                    },
                },
                token : device.device.notification_token,
            };

            let date = new Date(notification_time);
            // date.setSeconds(date.getSeconds()+10);

            //TODO: change cron to REDIS
            const job = new CronJob(date, function() {
                FCM.send(message, function(err, response) {
                    if(err){
                        console.log('error found', err);
                    }else {
                        console.log('response here', response);
                    }
                })
            }, null, true, 'Europe/Kiev');
            job.start();


            let taskJobs = cronJobs.filter(task => task._id === taskId);

            if (taskJobs.length === 0)
                cronJobs.push({_id: taskId, jobs: []});

            cronJobs.forEach((task) => {
                if (task._id === taskId) {
                    task['jobs'].push(job);
                }
            });

            exports.update_a_task(req,res);
        }
    });
};