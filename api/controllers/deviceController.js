'use strict';

const mongoose = require('mongoose'),
    Device = mongoose.model('Device');

exports.list_all_device = function(req, res) {
    Device.find({}, function(err, task) {
        if (err)
            res.send(err);
        res.json(task);
    });
};


exports.create_device = function(req, res) {
    let { uuid } = req.body;
    Device.findOne({ uuid: uuid }, function(err, device) {
        if (err || !device) {
            let new_device = new Device(req.body);
            new_device.save(function(err, device) {
                if (err)
                    res.send(err);
                res.json(device);
            });
        }else {
            res.json(device);
        }
    });

};


exports.read_device = function(req, res) {
    let { uuid } = req.params;
    Device.findOne({ uuid: uuid }, function(err, device) {
        if (err)
            res.send(err);
        res.json([device]);
    });
};


exports.update_device = function(req, res) {
    let { uuid } = req.body;
    Device.findOneAndUpdate({uuid: uuid}, req.body, {new: true}, function(err, device) {
        if (err)
            res.send(err);
        res.json(device);
    });
};


exports.delete_device = function(req, res) {
    let { uuid } = req.params;
    Device.remove({
        uuid: uuid
    }, function(err, device) {
        if (err) {
            res.send(err);
        }
        res.json({ message: 'ok' });
    });
};